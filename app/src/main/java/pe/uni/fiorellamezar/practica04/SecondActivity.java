package pe.uni.fiorellamezar.practica04;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class SecondActivity extends AppCompatActivity {
    Spinner spinnerCantidad;
    TextView textViewFoodSelected;
    EditText editTextNamePerson, editTextAddressPerson;
    ArrayAdapter<CharSequence> adapter;
    RadioGroup radioGroup;
    RadioButton radioButtonVisa, radioButtonEfectivo;
    Button buttonSend;
    SharedPreferences sharedPreferences;

    int cantidad;
    String namePerson, addressPerson, metodoPago;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        spinnerCantidad = findViewById(R.id.spinner);
        editTextNamePerson = findViewById(R.id.edit_text_name);
        editTextAddressPerson = findViewById(R.id.edit_text_address);
        textViewFoodSelected = findViewById(R.id.text_view_food_selected);
        radioGroup = findViewById(R.id.radio_group);
        radioButtonVisa = findViewById(R.id.radio_button_visa);
        radioButtonEfectivo = findViewById(R.id.radio_button_efectivo);
        buttonSend = findViewById(R.id.button_send);

        Intent intent = getIntent();

        //TEXT VIEW - COMIDA SELECCIONADA
        String foodSelected = intent.getStringExtra("TEXT_FOOD");
        textViewFoodSelected.setText(foodSelected);

        //SPINNER
        adapter = ArrayAdapter.createFromResource(this, R.array.numbers, android.R.layout.simple_spinner_item);
        spinnerCantidad.setAdapter(adapter);
        spinnerCantidad.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(position == 0){
                    cantidad = 1;
                    return;
                }
                if(position == 1){
                    cantidad = 2;
                    return;
                }
                if(position == 2){
                   cantidad = 3;
                   return;
                }
                if(position == 3){
                    cantidad = 4;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        // EDIT TEXT - DATOS INGRESADOS
        namePerson = editTextNamePerson.getText().toString();
        addressPerson = editTextAddressPerson.getText().toString();

        //RADIO BUTTON
        if(radioButtonEfectivo.isChecked()){
            metodoPago = "VISA";
        }
        if(radioButtonVisa.isChecked()) {
            metodoPago = "EFECTIVO";
        }


        if(namePerson.equals("") || addressPerson.equals("") || cantidad == 0){
            if(!radioButtonVisa.isChecked() && !radioButtonEfectivo.isChecked()){
                //Snackbar
                
            }
        }

       //retrieveData();
    }


    @Override
    protected void onPause() {
        super.onPause();
        //saveData();
    }


    private void saveData(){
        sharedPreferences = getSharedPreferences("saveData", Context.MODE_PRIVATE);
        namePerson = editTextNamePerson.getText().toString();
        addressPerson = editTextAddressPerson.getText().toString();
        if(radioButtonEfectivo.isChecked()){
            metodoPago = "VISA";
        }
        if(radioButtonVisa.isChecked()) {
            metodoPago = "EFECTIVO";
        }
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("key name", namePerson);
        editor.putString("key address", addressPerson);
        editor.putInt("key cantidad", cantidad);
        editor.putString("key pago", metodoPago);
        editor.apply();

    }
    private void retrieveData(){
        sharedPreferences = getSharedPreferences("saveData", Context.MODE_PRIVATE);
        namePerson = sharedPreferences.getString("key name", null);
        addressPerson = sharedPreferences.getString("key message", null);
        cantidad = sharedPreferences.getInt("key cantidad", 0);
        metodoPago = sharedPreferences.getString("key pago", null);

        editTextNamePerson.setText(namePerson);
        editTextAddressPerson.setText(addressPerson);

        if(metodoPago.equals("VISA") ){
            radioButtonVisa.setChecked(true);
        }
        if (metodoPago.equals("EFECTIVO")){
            radioButtonEfectivo.setChecked(true);
        }

    }

}