package pe.uni.fiorellamezar.practica04;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    GridView gridView;
    ArrayList<String> text = new ArrayList<>();
    ArrayList<Integer> image = new ArrayList<>();
    ArrayList<String> description = new ArrayList<>();

    String titleFood;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        gridView = findViewById(R.id.grid_view);
        fillArray();

        GridAdapter gridAdapter = new GridAdapter(this, text, image, description);
        gridView.setAdapter(gridAdapter);

        gridView.setOnItemClickListener((parent, view, position, id) -> {
            titleFood = text.get(position);
            Intent intent = new Intent(MainActivity.this, SecondActivity.class);
            intent.putExtra("TEXT_FOOD", titleFood);
            startActivity(intent);
        });

    }
    private void fillArray(){
        text.add("Anticucho");
        text.add("Arroz con pollo");
        text.add("Cuy frito");
        text.add("Frejoles");
        text.add("Hamburguesa");
        text.add("Lomo");
        text.add("Papa rellena");
        text.add("Tacos");

        image.add(R.drawable.anticucho);
        image.add(R.drawable.arrozconpollo);
        image.add(R.drawable.cuy);
        image.add(R.drawable.frejoles);
        image.add(R.drawable.hamburguesa);
        image.add(R.drawable.lomo);
        image.add(R.drawable.paparellena);
        image.add(R.drawable.tacos);

        description.add(getResources().getString(R.string.text_view_food_description));
        description.add(getResources().getString(R.string.text_view_food_description));
        description.add(getResources().getString(R.string.text_view_food_description));
        description.add(getResources().getString(R.string.text_view_food_description));
        description.add(getResources().getString(R.string.text_view_food_description));
        description.add(getResources().getString(R.string.text_view_food_description));
        description.add(getResources().getString(R.string.text_view_food_description));
        description.add(getResources().getString(R.string.text_view_food_description));

    }
}